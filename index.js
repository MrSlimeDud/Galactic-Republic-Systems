// Requires dependencies
const Discord = require("discord.js");
const fs = require("fs");
const config = require("./config.json")


// Creates a new client, and ignores the everyone ping
const bot = new Discord.Client({disableEveryone: true});

// Requires the functions
require("./Modules/functions.js")(bot);

// Creates a new collection for the commands
bot.commands = new Discord.Collection();

// Sets up the config
let authToken = config.discordToken;
let prefix = config.prefix;
let robloxUser = config.robloxUsername;
let robloxPass = config.robloxPassword;


// Reads each command file and imports it so it can be run later in the script
fs.readdir("./Commands/", (err, files) => {
    if (err) console.log(err);
    let jsfile = files.filter(f => f.split(".").pop() === "js")

    if (jsfile.length <= 0) {
        console.log("Unable to find commands...")
        return;
    }
    jsfile.forEach ((f, i) => {
        let props = require(`./Commands/${f}`);
        bot.log("log", `${f} loaded!`, "Load");
        bot.commands.set(props.help.name, props);
    });

});

// When the bot is ready, log to the console and set the activity
bot.on("ready", async () => {
    console.log(`${bot.user.username} is online on ${bot.guilds.size} servers!`)
    bot.user.setActivity("to prefix: '-'!", {type: "LISTENING"});
});

// When a message is sent, check if it is a command for a certain file. If it is, run the command.
bot.on("message", async message => {
    if (message.author.bot) return;
    if (message.channel.type === "dm") return;

    let messageArray = message.content.split(" ");
    let cmd = messageArray[0];
    let args = messageArray.slice(1);

    let commandfile = bot.commands.get(cmd.slice(prefix.length));
    if (commandfile) commandfile.run(bot, message, args);

});

// Login with the token
bot.login(authToken);
