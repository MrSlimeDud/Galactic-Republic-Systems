module.exports = (bot) => {
    
    // Special logging for cosmetics and organization. Future patches may include time and colors.
    bot.log = (type, msg, title) => {
        if (!title) title = "Log";
        console.log(`[${type}] [${title}] ${msg}`);
      };

    // These 2 process methods will catch exceptions and give *more details* about the error and stack trace.
    process.on("uncaughtException", (err) => {
        const errorMsg = err.stack.replace(new RegExp(`${__dirname}/`, "g"), "./");
        console.error("Uncaught Exception: ", errorMsg);
        // Always best practice to let the code crash on uncaught exceptions. 
        // Because you should be catching them anyway.
        process.exit(1);
    });

    process.on("unhandledRejection", err => {
        console.error("Uncaught Promise Error: ", err);
    });
}